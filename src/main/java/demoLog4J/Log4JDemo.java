package demoLog4J;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Log4JDemo {

	public static Logger logger= LogManager.getLogger();
	
	public static void main(String[] args) {
		
		System.out.println("\n hello world...! \n");
		
		logger.info("This is a information message");
		logger.error("This is an error message");
		logger.warn("This is a warning  message");
		logger.fatal("This is a fatal message");
		
		System.out.println("completed");
		

	}

}
