package test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class TestNGdemo {
	
	WebDriver driver=null;
	@BeforeTest
	public void setupTest(){
		
		WebDriverManager.chromedriver().setup();   
		driver = new ChromeDriver();
	}
	
	@Test
	public void testDemo(){
		driver.get("https://google.com");
	}
	
	@AfterTest
	public void tearDownTest(){
		driver.close();
		System.out.println("test completed");
	}

}
