package test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;
import pageObjectModel.GoogleSearchObjectPage;

public class GoogleSearchObjectPageTest {

	static WebDriver driver=null;

	@BeforeTest
	public void setupTest(){
		WebDriverManager.chromedriver().setup();   
		driver = new ChromeDriver();

	}	


	@Test
	public static void googleSearchObjectPageTest(){

		GoogleSearchObjectPage obj= new GoogleSearchObjectPage(driver);
		driver.get("https://google.com");

		obj.insertText("test");

		//	obj.clickSearcButton();

	}

	@AfterTest
	public void teatDownTest(){
		driver.close();
		System.out.println("Browser has closed successfully");
	}


}
