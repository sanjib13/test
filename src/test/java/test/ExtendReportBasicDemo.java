package test;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.aventstack.extentreports.ExtentReporter;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.sun.corba.se.impl.oa.poa.ActiveObjectMap.Key;

import io.github.bonigarcia.wdm.WebDriverManager;

public class ExtendReportBasicDemo {
	
	static WebDriver driver=null;
	
	public static void main(String[] args) {
		// start reporters
        ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter("extent.html");
    
        // create ExtentReports and attach reporter(s)
        ExtentReports extent = new ExtentReports();
        extent.attachReporter(htmlReporter);
        
     // creates a toggle for the given test, adds all log events under it    
        ExtentTest test = extent.createTest("MyFirstTest", "Sample description");

        WebDriverManager.chromedriver().setup();   
		driver = new ChromeDriver();
		
		// log(Status, details)
        test.log(Status.INFO, "Starting test Case");
        driver.get("https://google.com");
        test.pass("Navigated to Google.com");
        
		driver.findElement(By.name("q")).sendKeys("hello");
		test.pass("Entered text in search box");
		
		//driver.findElement(By.name("btnk")).sendKeys(Keys.RETURN);
		//test.pass("Pressed key Enter key");
		
		driver.close();
		driver.quit();
		test.pass("Close the Browser");
		
		test.info("test completed");
		
		// calling flush writes everything to the log file
        extent.flush();
	}

}
