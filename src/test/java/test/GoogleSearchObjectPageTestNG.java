package test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;
import pageObjectModel.GoogleSearchObjectPage;

public class GoogleSearchObjectPageTestNG {
	
	private static WebDriver driver=null;
	
	public static void main(String[] args) {
		
		googleSearchObjectPageTest();
		
	}
	
	public static void googleSearchObjectPageTest(){
		
		WebDriverManager.chromedriver().setup();   
		driver = new ChromeDriver();
		
		GoogleSearchObjectPage obj= new GoogleSearchObjectPage(driver);
		driver.get("https://google.com");
		
		obj.insertText("test");
		
	//	obj.clickSearcButton();
		
		driver.close();
	}

}
