package test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DesiredCapabilityDemo {
	
	static WebDriver driver=null;
	
	public static void main(String[] args) {
		
		DesiredCapabilities caps=new DesiredCapabilities();
		caps.setCapability("ignoreProtectedModeSettings", true);
		
		WebDriverManager.iedriver().setup();
		driver = new InternetExplorerDriver(caps);
		driver.get("https://google.com");
		driver.close();
		

	}

}
