package pageObjectModel;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class GoogleSearchObjectPage {
	
	WebDriver driver=null;
	
	By textbox_search = By.name("q");
	By button_search = By.name("btnk");
	
	 
	public GoogleSearchObjectPage(WebDriver driver){
		
		this.driver=driver;
		
	}
	
	

	public void insertText(String text){
		  driver.findElement(textbox_search).sendKeys("text");
		  
	  }
	  
	  public void clickSearcButton (){
		  driver.findElement(button_search).sendKeys(Keys.RETURN);
	  }
		  
	  

}
